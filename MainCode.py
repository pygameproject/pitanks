import os
import sys

import pygame

FPS = 50


def load_image(name):
    fullname = os.path.join('data', name)
    if not os.path.isfile(fullname):
        print(f"Файл с изображением '{fullname}' не найден")
        sys.exit()
    image = pygame.image.load(fullname)
    return image


def terminate():
    pygame.quit()
    sys.exit()


def start_screen():
    pygame.init()
    intro_text = ["Описание игры",
                  "игра для двух игроков",
                  "в ней всего 3 уровня",
                  "цель - попасть в противника",
                  "кто больше одержит побед в 3-х раундов, тот выйграл",
                  "1 игрок управляет с помощью кнопок 'a', 'w', 'd', 's' и стреляет с помощью кнопки 'space'",
                  "2 игрок управляет с помощью стрелок и стреляет с помощью кнопки 'enter'"]
    name_game = ["PiTankS"]
    start_the_game = ["Нажми для продолжения"]
    fon = pygame.transform.scale(load_image('fon.jpg'), (1500, 900))
    screen.blit(fon, (0, 0))
    text_coord = 10
    for line in intro_text:
        recording(line, text_coord, 45, "green", 10)
        text_coord += 40
    recording(name_game[0], 20, 80, "red", 1265)
    recording(start_the_game[0], 800, 80, "blue", 400)
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                terminate()
            elif event.type == pygame.KEYDOWN or \
                    event.type == pygame.MOUSEBUTTONDOWN:
                return
        pygame.display.flip()
        clock.tick(FPS)


def recording(line, text_coord, font, color, rect_x):
    font = pygame.font.Font(None, font)
    string_rendered = font.render(line, True, pygame.Color(color))
    intro_rect = string_rendered.get_rect()
    intro_rect.top = text_coord
    intro_rect.x = rect_x
    text_coord += intro_rect.height
    screen.blit(string_rendered, intro_rect)


size = width, height = 1500, 900
screen = pygame.display.set_mode(size)

pygame.display.set_caption('Танчики')
clock = pygame.time.Clock()
start_screen()

running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
    screen.fill((0, 0, 0))
    pygame.display.flip()
    clock.tick(FPS)